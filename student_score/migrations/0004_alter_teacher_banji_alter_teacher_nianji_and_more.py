# Generated by Django 4.1.3 on 2022-11-27 10:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student_score', '0003_teacher_schoolname'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='banji',
            field=models.CharField(max_length=16, verbose_name='班级'),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='nianji',
            field=models.CharField(max_length=16, verbose_name='年级'),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='xueke',
            field=models.CharField(max_length=16, verbose_name='任课教师'),
        ),
    ]
