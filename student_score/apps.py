from django.apps import AppConfig


class StudentScoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'student_score'
